describe 'Application file' do
  context 'String#minus' do
    let!(:str) { 'Once upon a time I saw a dragon' }

    it 'removes the entire word' do
      expect(str.minus('time')).to eq('Once upon a  I saw a dragon')
    end

    it 'removes a part of the word' do
      expect(str.minus('Oe')).to eq('nc upon a tim I saw a dragon')
    end

    it 'removes all given symbols from the string' do
      expect(str.minus('mes')).to eq('Onc upon a ti I aw a dragon')
    end
  end
end
